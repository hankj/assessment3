var express = require("express");
var User = require("./query_db"); // livenow_db.js support all queries to livenow database
var watch = require("connect-ensure-login");

module.exports = function (app) {

    //node server ensures all client requested routes with "/api" wil lrequire login before further processing
    app.use("/api", watch.ensureLoggedIn("/status/401"));   

    app.get("/api/user/me", function (req, res) {
        console.log("In /api/user/me - req.user: ",req.user );
        res.status(200).json(req.user);
    });
    
    //client request for all users
    app.get("/api/users", function (req, res) {
        User.findAll()
            .then(function (users) {
                console.log("SQL query OK", users);
                res.status(200).json(users);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            })
    });

    
    //client request for details of particular user using user id
    app.get("/api/user/:userId", function (req, res) {
        console.log("Server: Request details of user id :", req.params.userId);
        User.findOneById([req.params.userId])  // keep the [] for sql '?' variable insert(s)
            .then(function (user) {
                console.log("Server: Fetched user details: ",user[0]);
                res.status(200).json(user[0]);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            })
    });

    //client request for details of particular channel using ytvideo Id
    app.get("/api/channel/:videoId", function (req, res) {
        console.log("Server: Request details of channel id :", req.params.videoId);
        User.findOneByYtVideoId([req.params.videoId])  // keep the [] for sql '?' variable insert(s)
            .then(function (channel) {
                console.log("Server: Fetched channel details: ",channel[0]);
                res.status(200).json(channel[0]);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            })
    });


    //client save details of particular user using user id
    app.put("/api/user/save", function (req, res) {
        console.log("Server: User details to save:", req.body.params);
        User.saveOne([req.body.params.firstname,
            req.body.params.lastname,
            req.body.params.username,
            req.body.params.email,
            req.body.params.active,
            req.body.params.userid          //id is not saved but used for locating record to save
        ])  // keep the [] for sql '?' variable insert(s)
            .then(function (user) {
                console.log("Server: Save user details successful");
                res.status(200).send("Save success for user id"+req.body.params.id);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            })
    });


    //client save my profile
    app.put("/api/user/saveprofile", function (req, res) {
        console.log("Server: My profile to save:", req.body.params);
        User.saveOneMyProfile([req.body.params.firstname,
            req.body.params.lastname,
            req.body.params.username,
            req.body.params.password,
            req.body.params.email,
            req.body.params.active,
            req.body.params.userid          //id is not saved but used for locating record to save
        ])  // keep the [] for sql '?' variable insert(s)
            .then(function (user) {
                console.log("Server: Save my profile successful");
                res.status(200).send("Save my profile success for user id"+req.body.params.id);
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            })
    });
    
    
    
    //new user registration of details 
    app.post("/user/register", function (req, res) {
        console.log("Server: New User details to Register:", req.body.params);
        User.registerOne([req.body.params.firstname,
            req.body.params.lastname,
            req.body.params.username,
            req.body.params.password,
            req.body.params.email,
            req.body.params.active
            // req.body.params.userid          //no id to save as new user
        ])  // keep the [] for sql '?' variable insert(s)
            .then(function (user) {
                console.log("Server: Register new user details successful");
                res.status(200).send("Register new success");
            })
            .catch(function (err) {
                console.log(err);
                res.status(500).end();
            })
    });



    app.use(express.static(__dirname + "/public"));
    app.use("/bower_components", express.static(__dirname + "/bower_components"));
};