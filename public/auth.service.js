(function () {
    angular.module("MyApp")
        .service("AuthService", AuthService);

    function AuthService ($http, $rootScope) {
        var self = this;
        var currentUser;


        $http.get("/api/user/me")
            .then(function (user) {
                if(user){
                    $rootScope.isLoggedIn = true;
                    currentUser = user;
                }
            });
        self.getCurrentUser = function () {
            return currentUser;
        }
    }
    AuthService.$inject = ["$http", "$rootScope"];
})();