(function (){
    angular.module("MyApp")
        .controller("NavigationCtrl", NavigationCtrl);

        // .controller("FirstController", function ($scope, $rootScope) {
        //     $scope.$on("SOME_EVENT" , function (e, value) {
        //         console.log("First COntroller", value);
        //     });
        //     $rootScope.$broadcast("SOME_EVENT", "FROM_FIRST - using root scope");
        //     $scope.$emit("SOME_EVENT", "FROM_FIRST - using scope")
        // })
        // .controller("SecondController", function ($scope, $rootScope) {
        //     $scope.$on("SOME_EVENT" , function (e, value) {
        //         console.log("SecondController", value );
        //     });
        //     $rootScope.$emit("SOME_EVENT", "FROM_SECOND - using root scope");
        //     $scope.$emit("SOME_EVENT", "FROM_SECOND - using scope")
        // });

    function NavigationCtrl($rootScope, AuthService) {
        var vm =this;
        vm.page = {
          listPage: false,
          detailPage: false
        };

        console.log("In Navigation Ctrl");

        vm.onPage = function () {
            if ( document.URL.contains("list.html") ) {
                console.log("On List.html page");
                return true;                
            }else {
                console.log("On Non List.html page");
                return false;
            }

        };
        
        AuthService.getCurrentUser();
        vm.isLoggedIn = function () {
            console.log("$rootScope.isLoggedIn :",$rootScope.isLoggedIn);
            return $rootScope.isLoggedIn;
        }
    }
    NavigationCtrl.$inject = ["$rootScope","AuthService"];

})();