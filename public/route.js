(function () {

    angular
        .module("MyApp")
        .config(MyConfig);

    function MyConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("landing", {
                url: "/landing",
                templateUrl: "/views/landing.html",
                controller: "LandingCtrl as ctrl"
            })
            .state("login", {
                url: "/login",
                templateUrl: "/views/login.html",
                controller: "LoginCtrl as ctrl"
            })
            .state("register", {
                url: "/register",
                templateUrl: "/views/register.html",
                controller: "RegisterCtrl as ctrl"
            })
            .state("actor_register", {
                url: "/actor_register",
                templateUrl: "/views/actor_register.html",
                controller: "ActorRegisterCtrl as ctrl"
            })
            .state("list", {
                url: "/list",
                templateUrl: "/views/list.html",
                controller: "ListCtrl as ctrl"
            })
            .state("detail", {
                url: "/detail/:userId",
                templateUrl: "/views/detail.html",
                controller: "DetailCtrl as ctrl"
            })
            .state("myprofile", {
                url: "/myprofile/:userId",
                templateUrl: "/views/myprofile.html",
                controller: "ProfileCtrl as ctrl"
            })
            .state("viewchannel", {
                url: "/viewchannel/:ytvideoId",
                templateUrl: "/views/youtube.html",
                controller: "ChannelCtrl as ctrl"
            })
            .state("logout", {
                url: "/logout",
                // template: "",
                controller: "LogoutCtrl as ctrl"
            });

        $urlRouterProvider.otherwise("/landing");
    }

    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

})();
