(function () {
    angular.module("MyApp")
        .service("dbService", dbService);

    function dbService($http, $q) {
        var vm = this;

        vm.listUsers = function (limit, offset) {
            var defer = $q.defer();
            var params = {
                limit: limit || 10,
                offset: offset || 0
            };

            //Request to node server to fetch all users from database
            $http.get("/api/users", {
                params: params
            }).then(function (users) {
                defer.resolve(users.data);
            }).catch(function (err) {
                defer.reject(err);
            });

            return defer.promise;
        };

        //request for user details to display using user id    
        vm.userDetails = function (userId) {
            var defer = $q.defer();

            $http.get("/api/user/" + userId)
                .then(function (user) {
                    defer.resolve(user.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };

        //request for my profile to display using user id
        vm.showProfile = function () {
            var defer = $q.defer();

            $http.get("/api/user/me")   //server already stored my details in req.user when login so no need params
                .then(function (user) {
                    defer.resolve(user.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };

        //save my profile details
        vm.SaveMyProfile = function (user) {
            var defer = $q.defer();

            $http.put("/api/user/saveprofile", {
                params: {firstname: user.first_name,
                    lastname: user.last_name,
                    username: user.user_name,
                    password: user.password,
                    email: user.email,
                    active: user.active,
                    userid: user.id
                }})
                .then(function (result) {
                    console.log(result);        //successful save result
                    defer.resolve(result);
                })
                .catch(function (errCode) {
                    defer.reject(errCode);
                });

            return defer.promise;
        };

        //request for a channel details     
        vm.channelDetails = function (ytVideoId) {
            var defer = $q.defer();

            $http.get("/api/channel/" + ytVideoId)
            // $http.get("/api/channel/" + "mx6t6E24SSM")
                .then(function (channel) {
                    defer.resolve(channel.data);
                })
                .catch(function (err) {
                    defer.reject(err);
                });

            return defer.promise;
        };
        

        //register user details     
        vm.RegisterUserDetails = function (user) {
            var defer = $q.defer();

            $http.post("/user/register", {
                params: {firstname: user.first_name,
                    lastname: user.last_name,
                    username: user.user_name,
                    password: user.password,
                    email: user.email,
                    active: user.active
                }})
                .then(function (result) {
                    console.log(result);        //successful save result
                    defer.resolve(result);
                })
                .catch(function (errCode) {
                    defer.reject(errCode);
                });

            return defer.promise;
        };


        //save for user details     
        vm.SaveUserDetails = function (user) {
            var defer = $q.defer();

            $http.put("/api/user/save", {
                 params: {firstname: user.first_name,
                          lastname: user.last_name,
                          username: user.user_name,
                          email: user.email,
                          active: user.active,
                          userid: user.id
                }})
                .then(function (result) {
                    console.log(result);        //successful save result
                    defer.resolve(result);
                })
                .catch(function (errCode) {
                    defer.reject(errCode);
                });

            return defer.promise;
        };



    }

    dbService.$inject = ["$http", "$q"];
})();

