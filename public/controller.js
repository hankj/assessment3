(function () {
    angular.module("MyApp")
        .controller("LandingCtrl", LandingCtrl)
        .controller("ListCtrl", ListCtrl)
        .controller("DetailCtrl", DetailCtrl)
        .controller("LoginCtrl", LoginCtrl)
        .controller("RegisterCtrl", RegisterCtrl)
        .controller("ActorRegisterCtrl", ActorRegisterCtrl)
        .controller("ChannelCtrl", ChannelCtrl)
        .controller("ProfileCtrl", ProfileCtrl)
        .controller("LogoutCtrl", LogoutCtrl);

    //Global $scope variables to support mobility among views
    // $rootScope.user_name = "";   //user name for display
    
    // **** LANDING CONTROLLER ********//
    function LandingCtrl(dbService, $stateParams, $scope, $state) {
        console.log("In Landing Controller");

        var vm = this;

        // $scope.$on("event:auth-loginRequired", function () {
        //     $state.go("login");
        // });
        //
        // $scope.$on("event:auth-loginConfirmed", function () {
        //     $rootScope.isLoggedIn = true;
        //     console.log("Login Confirmed")
        // });
        //
        // $scope.$on("event:auth-forbidden", function () {
        //     console.log("Forbidden");
        // });

    }
    LandingCtrl.$inject = ["dbService", "$stateParams", "$scope", "$state"];
    // **** LANDING CONTROLLER ********//


    // **** REGISTER USER CONTROLLER ********//
    function RegisterCtrl(dbService, $state, $rootScope) {
        console.log("In Register Controller");

        var vm = this;
        vm.user = [];
        vm.status = {
            message: "",
            code: 0
        };

        vm.registerUser = function(){
            console.log("In RegisterUser()");
            dbService.RegisterUserDetails(vm.user)
                .then(function (user) {
                    $rootScope.user_name = vm.user.user_name;
                    // console.log("$rootScope.user_name", $rootScope.user_name);
                    console.log("Success Registering User",user); //show the saved user details
                    vm.status.message = "Register user details successful";
                    vm.status.code = 200;
                    $state.go("actor_register");   //after user registration go to to actor registration
                    
                })
                .catch(function (errCode) {
                    vm.status.message = "Register user details fails";
                    vm.status.code = errCode;
                });
        }
    }
    RegisterCtrl.$inject = ["dbService","$state","$rootScope"];
    // **** REGISTER USER CONTROLLER ********//


    // **** ACTOR REGISTRATION CONTROLLER ********//
    function ActorRegisterCtrl(dbService, $rootScope, $state) {
        console.log("In Actor Registration Controller");

        var vm = this;
        vm.username = $rootScope.user_name;
        vm.registerActor = false;
        vm.user = [];
        vm.status = {
            message: "",
            code: 0
        };
        console.log("$rootScope.user_name", $rootScope.user_name);
        console.log("vm.user_name", vm.user_name);

        vm.actorRegister = function () {
            console.log("In ActorRegister()");

            // dbService.RegisterUserDetails(vm.user)
            //     .then(function (user) {
            //         console.log("Success Registering User",user); //show the saved user details
            //         vm.status.message = "Register user details successful";
            //         vm.status.code = 200;
            //         $state.go("actor_register");
            //
            //
            //     })
            //     .catch(function (errCode) {
            //         vm.status.message = "Register user details fails";
            //         vm.status.code = errCode;
            //     });

            console.log("User filled up Actor Registration Form");
            $state.go("login");

        };

        //user select to register as Actor  
        vm.userChooseActorReg = function () {
            console.log("User click button to register as actor");
            vm.registerActor = true;
        };

        //user select to register as Actor  
        vm.userContinuelogin = function () {
            console.log("User click button to continue to login");
            $state.go("login");         
        };
        
    }     
    ActorRegisterCtrl.$inject = ["dbService", "$rootScope", "$state"];
    // **** ACTOR REGISTRATION CONTROLLER ********//
    
    
    // **** LIST CONTROLLER ********//
    function ListCtrl(dbService, $stateParams, $scope, $state) {
        console.log("In List Controller");

        var vm = this;

        $scope.$on("event:auth-loginRequired", function () {
            $state.go("login");
        });

        $scope.$on("event:auth-loginConfirmed", function () {
            console.log("Login Confirmed")
        });

        $scope.$on("event:auth-forbidden", function () {
            console.log("Forbidden");
        });

        dbService.listUsers()
            .then(function (users) {
                vm.users = users;
                console.log("Success in List Controller:", vm.users);
            })
            .catch(function (err) {
                alert("Error in List Controller: requesting to show all users");
                console.log("Error in List Controller: requesting to show all users", err);
            });
    }
    ListCtrl.$inject = ["dbService", "$stateParams", "$scope", "$state"];
    // **** LIST CONTROLLER ********//


    // **** CHANNEL VIEW CONTROLLER ********//
    function ChannelCtrl($stateParams, dbService) {
        console.log("In Channel Controller: $stateParams.video_id", $stateParams.ytvideoId);
    
        var vm = this;
        vm.status = {
            message: "",
            code: 0
        };
    
        //use supporting dbService to 
        dbService.channelDetails($stateParams.ytvideoId)
            .then(function (channel) {
                vm.channel = channel;    //"user" hold the user details
                console.log("Get Channel Successful in Channel Controller:", vm.channel);
                console.log("In youtube player, Video ID :",vm.channel.ytvideo_id);
                vm.player = onYouTubeIframeAPIReady(vm.channel.ytvideo_id)
            });
    
        // vm.saveChannel = function(){
        //     console.log("In SaveChannel()");
        //     dbService.SaveChannelDetails(vm.user)
        //         .then(function (user) {
        //             console.log("Success Save Channel in Channel Controller",user); //show the saved user details
        //             vm.status.message = "Save channel details successful";
        //             vm.status.code = 200;
        //
        //         })
        //         .catch(function (errCode) {
        //             vm.status.message = "Save channel details fails";
        //             vm.status.code = errCode;
        //         });
    }
    ChannelCtrl.$inject = ["$stateParams", "dbService"];
    // **** CHANNEL VIEW CONTROLLER ********//


    // **** USER DETAIL CONTROLLER ********//
    function DetailCtrl($stateParams, dbService) {
        console.log("In Detail Controller: $stateParams.userId :",$stateParams.userId);

        var vm = this;
        vm.status = {
            message: "",
            code: 0
        };

        //use supporting dbService
        dbService.userDetails($stateParams.userId)
            .then(function (user) {
                vm.user = user;    //"user" hold the user details
                console.log("Success Fetch in Detail Controller:", vm.user);
            });

        vm.saveUser = function(){
            console.log("In SaveUser()");
            dbService.SaveUserDetails(vm.user)
                .then(function (user) {
                    console.log("Success Save in Detail Controller",user); //show the saved user details
                    vm.status.message = "Save user details successful";
                    vm.status.code = 200;

                })
                .catch(function (errCode) {
                    vm.status.message = "Save user details fails";
                    vm.status.code = errCode;
                });
        }
    }
    DetailCtrl.$inject = ["$stateParams", "dbService"];
    // **** USER DETAIL CONTROLLER ********//

    // **** MY PROFILE CONTROLLER ********//
    function ProfileCtrl($stateParams, $state, dbService) {
        console.log("In Profile Controller: $stateParams.userId :",$stateParams.userId);

        var vm = this;
        vm.status = {
            message: "",
            code: 0
        };
        
        //use supporting dbService 
        dbService.showProfile()
            .then(function (user) {
                vm.user = user[0];    //"user" hold the user details
                console.log("Success Fetch in Profile Controller:", vm.user);
            });

        vm.saveProfile = function(){
            console.log("In SaveProfile()");
            dbService.SaveMyProfile(vm.user)
                .then(function (user) {
                    console.log("Success Save in Profile Controller",user); //show the saved user details
                    vm.status.message = "Save my profile successful";
                    vm.status.code = 200;
                    $state.go("list");

                })
                .catch(function (errCode) {
                    vm.status.message = "Save my profile fails";
                    vm.status.code = errCode;
                });


        }
    }
    ProfileCtrl.$inject = ["$stateParams", "$state", "dbService"];
    // **** MY PROFILE CONTROLLER ********//



    // **** LOGIN CONTROLLER ********//
    function LoginCtrl($http, authService, $state, $rootScope){
        console.log("In Login Controller");

        var vm = this;
        vm.user = {};

        vm.login = function () {
            $http.post("/login", vm.user)
                .then(function () {
                    console.log("Login Successful!");
                    $rootScope.isLoggedIn = true;       //global variable to inform user already login
                    $state.go("list");
                    authService.loginConfirmed();
                })
                .catch(function () {
                    vm.message = "Login not successful"
                });
        };

    }
    LoginCtrl.$inject = ["$http", "authService", "$state", "$rootScope"];
    // **** LOGIN CONTROLLER ********//

    // **** LOGOUT CONTROLLER ********//
    function LogoutCtrl($http, $state, $rootScope){
        console.log("In Logout Controller");

        var vm = this;

            $http.get("/logout")
                .then(function () {
                    console.log("Logout Successful!");
                    $rootScope.isLoggedIn = false;
                })
                .catch(function () {
                    console.log("Logout Fails!");
                });

        $state.go("landing");
    }
    LogoutCtrl.$inject = ["$http", "$state", "$rootScope"];
    // **** LOGOUT CONTROLLER ********//

    
}());


//*******8 Helper functions **********//

function onYouTubeIframeAPIReady(videoId) {
    return new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: videoId,
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange
        },
        playerVars: {
            'autoplay': 1,          //auto play when ready
            'cc_load_policy': 0,    //off caption
            'controls': 0,          // disable player controls
            'disablekb': 0,         // disable keyboard as player controls
            'enablejsapi': 1,       // enables the player to be controlled via IFrame or JS Player API calls
            'fs': 0,                // prevents the fullscreen button from display
            'hl': 'zh',             // ISO 639-1 two-letter language code
            'iv_load_policy': 1,    // 1 show video annotations,3 no
            'loop': 0,              // 1 loops, 0 do not loop
            'modestbranding': 0    // 1 prevent the YouTube logo from displaying in the control bar
        }
    });
}


// The YT API will call this function when the video player is ready.
function onPlayerReady(event) {
    event.target.playVideo();
}

//     The YT API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player play for X seconds and then stop.
var done = false;
function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
        setTimeout(stopVideo, 6000);  //play for 6sec then stop
        done = true;
    }
}
function stopVideo() {
    vm.player.stopVideo();
}
//****** youtube js code  *******//

