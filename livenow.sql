-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: livenow
-- ------------------------------------------------------
-- Server version	5.7.13-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actor_user`
--

DROP TABLE IF EXISTS `actor_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `actor_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned DEFAULT NULL,
  `channel_id` int(10) unsigned DEFAULT NULL,
  `actor_photo` blob,
  PRIMARY KEY (`id`),
  KEY `channel_id` (`channel_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `actor_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `actor_user_ibfk_2` FOREIGN KEY (`channel_id`) REFERENCES `channel` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actor_user`
--

LOCK TABLES `actor_user` WRITE;
/*!40000 ALTER TABLE `actor_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `actor_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channel`
--

DROP TABLE IF EXISTS `channel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `channel` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `actor_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `subscriber_count` int(10) unsigned DEFAULT NULL,
  `view_count` int(10) unsigned DEFAULT NULL,
  `rating` enum('G','R','PG','PG-13','NC-17') DEFAULT NULL,
  `accept_gift` enum('Y','N') DEFAULT NULL,
  `accept_subscription` enum('Y','N') DEFAULT NULL,
  `channel_photo` blob,
  `ytvideo_id` varchar(45) DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ytvideo_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `title` (`title`),
  KEY `actor_id` (`actor_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `channel_ibfk_1` FOREIGN KEY (`actor_id`) REFERENCES `actor_user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `channel_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channel`
--

LOCK TABLES `channel` WRITE;
/*!40000 ALTER TABLE `channel` DISABLE KEYS */;
INSERT INTO `channel` VALUES (2,'2016-08-14 23:40:36','2016-08-15 00:34:47',NULL,'The Chillhop Cafe · 24/7 Live Radio · Chilled \' Jazzy \' Hip Hop Beats','The Chillhop Cafe is live streaming the best Chillhop 24 / 7! ○ If you like this stream, give it a like so more people will find it! :) ○ We have a bot in the chat to ...',123,321,'G',NULL,NULL,NULL,'mx6t6E24SSM',4,'https://i.ytimg.com/vi/mx6t6E24SSM/default_live.jpg'),(3,'2016-08-14 23:49:24','2016-08-14 23:49:24',NULL,'The Wild Safari: Africa 2016','The 24/7 video of a trip the wilder parts of Africa to be close nature',30,12,'G',NULL,NULL,NULL,'KF47Za1lfjM',5,NULL);
/*!40000 ALTER TABLE `channel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facebook_user`
--

DROP TABLE IF EXISTS `facebook_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facebook_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned DEFAULT NULL,
  `facebook_id` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `user_name` varchar(16) DEFAULT NULL,
  `display_name` varchar(100) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `age` tinyint(3) unsigned DEFAULT NULL,
  `bio` varchar(100) DEFAULT NULL,
  `photo` blob,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `address_id` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `facebook_id` (`facebook_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `facebook_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facebook_user`
--

LOCK TABLES `facebook_user` WRITE;
/*!40000 ALTER TABLE `facebook_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `facebook_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `google_user`
--

DROP TABLE IF EXISTS `google_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `google_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(10) unsigned DEFAULT NULL,
  `google_id` int(10) unsigned DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `user_name` varchar(16) DEFAULT NULL,
  `display_name` varchar(100) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  `gender` varchar(20) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `age` tinyint(3) unsigned DEFAULT NULL,
  `bio` varchar(100) DEFAULT NULL,
  `photo` blob,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `address_id` smallint(5) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `google_id` (`google_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `google_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `google_user`
--

LOCK TABLES `google_user` WRITE;
/*!40000 ALTER TABLE `google_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `google_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `user_name` varchar(16) NOT NULL,
  `PASSWORD` varchar(32) NOT NULL,
  `email` varchar(32) NOT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `address_id` smallint(5) unsigned DEFAULT NULL,
  `actor_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `actor_id` (`actor_id`),
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`actor_id`) REFERENCES `actor_user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'2016-08-13 15:21:14','2016-08-14 00:17:19','first1','last','firstlast1','123','a1@b.com',1,NULL,NULL),(2,'2016-08-13 15:22:31','2016-08-13 15:22:31','first2','last2','firstlast2','123','a2@b.com',NULL,NULL,NULL),(3,'2016-08-13 15:56:24','2016-08-13 15:56:24','first2a','last2a','firstlast2a','123','a2@b.com',NULL,NULL,NULL),(4,'2016-08-14 17:41:26','2016-08-15 17:22:24','kj1','han1','hankj1','12345678','hankj1@gmail.com',0,NULL,NULL),(5,'2016-08-14 17:43:53','2016-08-14 17:43:53','kj2','han2','hankj2','87654321','hankj2@yahoo.com',0,NULL,NULL),(6,'2016-08-14 17:51:36','2016-08-14 17:51:36','kj3','han3','hankj3','sdsdsdsds','hankj3@aaa.com',1,NULL,NULL),(7,'2016-08-14 17:54:33','2016-08-14 17:54:33','kj4','han4','hankj4','asasasasas','hankj4@bbb.com',1,NULL,NULL),(8,'2016-08-14 17:57:43','2016-08-14 17:57:43','kj5','han5','hankj5','wewewewewe','hankj5@gmail.com',1,NULL,NULL),(9,'2016-08-14 18:00:04','2016-08-14 18:00:04','kj6','han6','hankj6','sdssdsddsd','hankj6@q.co',1,NULL,NULL),(10,'2016-08-14 18:03:38','2016-08-14 18:03:38','kj6','han6','hankj6','sdsdssdsd','hankj6@a.com',1,NULL,NULL),(11,'2016-08-14 18:06:06','2016-08-14 18:06:06','kj7','han7','hankj7','asasaasa','hankj7@w.co',1,NULL,NULL),(12,'2016-08-14 18:09:40','2016-08-14 18:09:40','kj8','han8','hankj8','sdfsfsdfdsfsdfsdfs','hankj8@s.com.sg',1,NULL,NULL),(13,'2016-08-14 18:18:41','2016-08-14 18:18:41','kj9','han9','hankj9','asasasasa','hankj9@a.in',1,NULL,NULL),(14,'2016-08-14 18:26:29','2016-08-14 18:26:29','kj11','han11','hankj11','asassdsfdfqwq','hankj11@d.com',1,NULL,NULL),(15,'2016-08-14 18:54:15','2016-08-14 18:54:15','kk1','lim1','limkk1','asasasasasa','limkk1@qqq.com',1,NULL,NULL),(16,'2016-08-14 18:56:01','2016-08-15 17:22:11','kk2','lim2','limkk2','87654321','limkk2@ded.com',0,NULL,NULL),(17,'2016-08-14 19:00:36','2016-08-14 19:00:36','kk3','lim3','limkk3','asasasas','limkk3@a.g',0,NULL,NULL),(18,'2016-08-14 19:04:32','2016-08-14 19:04:32','kk4','lim4','limkk4','qwqwqwqwqwqw','limkk4@s.ff',0,NULL,NULL),(19,'2016-08-14 19:06:12','2016-08-14 19:06:12','kk5','lim5','limkk5','asdasdasdasda','limkk5@33.de',1,NULL,NULL),(20,'2016-08-14 19:11:27','2016-08-14 19:11:27','kk6','lim6','limkk6','dssdsdsdsdasasas','limkk6@fr.hg',0,NULL,NULL),(21,'2016-08-14 19:13:52','2016-08-14 19:13:52','kk7','lim7','limkk7','sddasdasdasd','limkk7@fe.gh',1,NULL,NULL),(22,'2016-08-14 19:15:43','2016-08-14 19:15:43','kk8','limk8','limkk8','sdsdsdsdsd','limkk8@as.frr',1,NULL,NULL),(23,'2016-08-14 19:22:32','2016-08-14 19:22:32','kk7','kk7','limkk7','qwqwqwqwqwqwqw','limkk7@ed.edu',1,NULL,NULL),(24,'2016-08-14 19:24:46','2016-08-14 19:24:46','kk8','lim8','limkk8','dfdfdfdfdfdfd','limkk8@rgrtg.df',0,NULL,NULL),(25,'2016-08-14 19:27:24','2016-08-14 19:27:24','kk9','lim9','limkk9','dfdsfdsfdsf','limkk9@de.dsf',0,NULL,NULL),(26,'2016-08-15 17:24:37','2016-08-15 17:24:37','121212','121212','12121212','12121212','21212@qww.d',0,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-15 17:51:27
