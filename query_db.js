var q = require('q');
var mysql = require("mysql");

var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "hankj",
    password: "philip01",
    database: "livenow",
    connectionLimit: 4
});


// const findAllQuery = "select film_id, title, release_year from film";
// const findOneQuery = "select * from film where film_id = ?";
// const findAllQuery = "SELECT id, user_name FROM livenow.user";
const findAllQuery = "SELECT user.id, user.user_name, channel.title, channel.subscriber_count, channel.view_count, " +
                     "channel.rating, channel.ytvideo_id, channel.ytvideo_link FROM livenow.user " +
                     "INNER JOIN livenow.channel ON user.id=channel.user_id";
const findOneByIdQuery = "SELECT * FROM livenow.user WHERE id = ? LIMIT 1";
const findOneByVideoIDQuery = "SELECT * FROM livenow.channel WHERE ytvideo_id = ? LIMIT 1";
const findOneByEmailQuery = "SELECT id, user_name, password, email FROM livenow.user WHERE email = ? LIMIT 1";
const saveOneQuery = "UPDATE livenow.user SET first_name = ? , last_name = ? , user_name = ? , " +
                     " email = ? , active = ? WHERE id = ? ";
const saveOneProfileQuery = "UPDATE livenow.user SET first_name = ? , last_name = ? , user_name = ? , password = ? , " +
                     " email = ? , active = ? WHERE id = ? ";
const registerOneQuery = "INSERT INTO livenow.user (first_name, last_name, user_name, password, email, active) VALUES" +
                    "( ? , ? , ?, ? , ?, ? )";

var makeQuery = function (sql, pool) {
    return function (args) {

        var defer = q.defer();

        pool.getConnection(function (err, connection) {
            if (err) {
                return defer.reject(err);
            }
            connection.query(sql, args || [], function (err, result) {
                connection.release();
                if (err) {
                    return defer.reject(err);
                }
                defer.resolve(result);
            })
        });

        return defer.promise;
    };
};


module.exports.findAll = makeQuery(findAllQuery, pool);
module.exports.findOneById = makeQuery(findOneByIdQuery, pool);
module.exports.findOneByYtVideoId = makeQuery(findOneByVideoIDQuery, pool);
module.exports.findByEmail = makeQuery(findOneByEmailQuery, pool);
module.exports.saveOne = makeQuery(saveOneQuery, pool);
module.exports.saveOneMyProfile = makeQuery(saveOneProfileQuery, pool);
module.exports.registerOne = makeQuery(registerOneQuery, pool);

