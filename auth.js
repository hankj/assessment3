var User = require("./query_db"); // livenow_db.js support all queries to livenow database

var LocalStrategy = require("passport-local").Strategy;

var GoogleStrategy = require("passport-google-oauth").OAuth2Strategy;

var FacebookStrategy = require("passport-facebook").Strategy;

//Setup local strategy
module.exports = function (app, passport) {

    function authenticate(username, password, done) {
        // User.findOne({user_name: email}, function(err, user){
        //     if (err) { return done(err);}
        //     if (!user) { return done(null, false); }
        //     if (!user.verifyPassword(password)) { return done(null, false); }
        //     return done(null, user);
        // });

        //Validate login email and userename from database
        var user = {id: 1};   //arbitrary user id value
        console.log("Server: Local Strategy: authentication");
        User.findByEmail([username])  //validate by querying database using user entered email
            .then(function (userInfo) {
                console.log("Server: Email query to database successful :", userInfo);
            //     return done(null, user = {id: 1});  //return user.id upon success
            // });
                var obj = JSON.stringify(userInfo);
                var user2verify = JSON.parse(obj);
                console.log("Server: User to verify as JS object", user2verify);
               
                console.log("Server: Entered password: %s Fetched > password %s user id %s", password, 
                            user2verify[0].password, user2verify[0].id);
            
                user = {id: user2verify[0].id};
                if (password == user2verify[0].password) {
                    console.log("Server- Authentication: password check successful");
                    return done(null, user);  //return user.id upon success
                }
                return done(null, false);   //authentication fails
            })
            .catch(function (err) {
                console.log("Server- Authentication: query fails error :", err);
                return done(null, false);   //fetch query fails i.e. no email match
            });

    } // local strategy authenticate function


    // callback function use by Google and Facebook validation
    function verifyCallback(accessToken, refreshToken, profile, done) {
        console.log("Access Token", accessToken);
        console.log("Refresh Token", refreshToken);
        console.log("Profile Info", profile);
        // User.findByEmail(profile.email, function(user) {
        //     // 1. New user'
        //     if(!user) {
        //         // Create a new user & add profile info
        //     }
        //
        //     // 2. User exists
        //     // Update profile info for user
        //
        //     done(null, user)
        // })
        done (null, profile); //pass email to req.user for serializer
        // done (null, profile.emails[0].value); //pass email to req.user for serializer
    }


    passport.use(new GoogleStrategy({
        clientID: "256314159550-tfjhrlea4j59lbcs2f7ha4b4ajn29igc.apps.googleusercontent.com",
        clientSecret: "C9FZNEs0GHtw5-B-obARuVJE",
        callbackURL: "http://localhost:3000/oauth/google/callback",
        profileFields:['id', 'displayName','name', 'gender', 'profileUrl' ]
    }, verifyCallback));

    passport.use(new FacebookStrategy({
        clientID: "520098204868051",
        clientSecret: "29719e0611c580032cb444a533097ae6",
        callbackURL: "http://localhost:3000/oauth/facebook/callback",
        profileFields:['id', 'displayName','name', 'gender', 'profileUrl' ]
    }, verifyCallback));


    passport.use(new LocalStrategy({
        usernameField: "email",
        passwordField: "password"
    }, authenticate));

    passport.serializeUser(function (user, done) {
        console.log("In serializUser() - user:", user);
        done(null, user.id)
    });

    passport.deserializeUser(function (userId, done) {
        console.log("In De-serializUser() - user ID:", userId);
        // Query the database for user with id
        User.findOneById([userId])
            .then(function (userInfo) {
                console.log("Server: query by ID to database successful :", userInfo);
                //     return done(null, user = {id: 1});  //return user.id upon success
                // });
                var obj = JSON.stringify(userInfo);
                var user = JSON.parse(obj);

                console.log("Server: Query by ID to database successful JS Object", user);
                done(null, user);  //return user.id upon success
            })
            .catch(function (err) {
                console.log("Server- DeserializeUser: DB query fails error :", err);
                return done(null, false);   //fetch query fails i.e. no email match
            });
    });




}; //export